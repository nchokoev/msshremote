package com.example.mremote;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class BrowseActivity extends Activity {

	private SSHRemote sshr = SSHRemote.getInstance();
	private Button btnBack;
	private ArrayAdapter<String> adapter;
	private ListView lvBrowse;
	private ArrayList<String> listItems=new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browse);
		this.lvBrowse = (ListView) this.findViewById(R.id.lvBrowse);
		adapter=new ArrayAdapter<String>(this,
	            android.R.layout.simple_list_item_1,
	            listItems);
	    this.lvBrowse.setAdapter(adapter);
		this.btnBack = (Button)this.findViewById(R.id.btnBack);
		this.btnBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(BrowseActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
		updateBrowseItems();
		this.lvBrowse.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				String text = ((TextView)arg1).getText().toString();
				sshr.cdOrOpen(text.replace("'", "\\'").replace(" ", "\\ ").replace("(", "\\(").replace(")", "\\)"));
				updateBrowseItems();
			}
		});
	}
	
	private void updateBrowseItems()
	{
		listItems.clear();
		listItems.addAll(sshr.refreshFolders());
		adapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.browse, menu);
		return true;
	}
	

}
