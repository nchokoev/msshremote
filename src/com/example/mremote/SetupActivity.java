package com.example.mremote;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SetupActivity extends Activity {
	private Button btnBack;
	private EditText hostAddress;
	private EditText userName;
	private EditText userPassword;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    this.setContentView(R.layout.activity_setup);
	    
		settings = this.getSharedPreferences(
			      "com.example.mremote", Context.MODE_PRIVATE);
		editor = settings.edit();
	    this.hostAddress = (EditText) this.findViewById(R.id.hostIP_field);
	    this.userName = (EditText) this.findViewById(R.id.username_field);
	    this.userPassword = (EditText) this.findViewById(R.id.password_field);
	    
	    this.hostAddress.setText(settings.getString("host_ip_address", "127.0.0.1"));
	    this.userName.setText(settings.getString("shell_username", ""));
	    this.userPassword.setText(settings.getString("shell_password", ""));
	    this.btnBack = (Button) this.findViewById(R.id.btnBack);
	    this.btnBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SetupActivity.this, MainActivity.class);
				editor.putString("host_ip_address", hostAddress.getText().toString());
				editor.putString("shell_username", userName.getText().toString());
				editor.putString("shell_password", userPassword.getText().toString());
				editor.commit();
				startActivity(intent);
			}
		});
	}

}
