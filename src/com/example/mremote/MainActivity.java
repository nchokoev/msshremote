package com.example.mremote;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity {
	private SSHRemote sshr = SSHRemote.getInstance();
	private String hostAddress;
	private String userName;
	private String userPassword;
	private Button btnConnect;
	private Button btnBrowse;
	private ImageButton btnPlay;
	private ImageButton btnPause;
	private ImageButton btnStop;
	private SharedPreferences settings;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		settings = this.getSharedPreferences(
			      "com.example.mremote", Context.MODE_PRIVATE);
		setContentView(R.layout.activity_main);
		Button btnSettings = (Button) this.findViewById(R.id.btn_Settings);
		btnSettings.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, SetupActivity.class);
				startActivity(intent);
			}
		});
		
	    this.btnConnect = (Button) this.findViewById(R.id.btnConnect);
	    this.btnConnect.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
			    hostAddress = settings.getString("host_ip_address", "127.0.0.1");
			    userName = settings.getString("shell_username", "");
			    userPassword = settings.getString("shell_password", "");
				sshr.connectHost(hostAddress, userName, userPassword);
				if(sshr.isConnected() == false){
					Toast.makeText(getApplicationContext(), 
                            "Connection failed!", Toast.LENGTH_LONG).show();
				}
			}
	    });
	    
	    this.btnBrowse = (Button) this.findViewById(R.id.btnBrowse);
	    this.btnBrowse.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, BrowseActivity.class);
				startActivity(intent);			
			}
		});
	    
	    this.btnPlay = (ImageButton) this.findViewById(R.id.btnPlay);
	    this.btnPlay.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				sshr.sendPlay();
			}
		});
	    
	    this.btnPause = (ImageButton) this.findViewById(R.id.btnPause);
	    this.btnPause.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				sshr.sendPause();
			}
		});
	    
	    this.btnStop = (ImageButton) this.findViewById(R.id.btnStop);
	    this.btnStop.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				sshr.sendStop();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
