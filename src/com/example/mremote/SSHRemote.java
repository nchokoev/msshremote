package com.example.mremote;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;

import com.jcraft.jsch.*;

public class SSHRemote {
	private static SSHRemote instance = null;
	private JSch jsch=new JSch();
	private Session session;
	private Channel channel;
	private String promptString = "<cp>$";
	private String nlPromptString1 = "\n<cp>$";
	private String nlPromptString2 = "\r<cp>$";
	PipedInputStream in;
	PipedOutputStream pin;

	PipedOutputStream out;
	PipedInputStream pout;
	BufferedReader consoleOutput;
	
	protected SSHRemote()
	{
		try {
			out = new PipedOutputStream();
			in = new PipedInputStream();
			pin = new PipedOutputStream(in);
			pout = new PipedInputStream(out);
			consoleOutput = new BufferedReader(new InputStreamReader(pout));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static SSHRemote getInstance()
	{
		if (instance == null)
		{
			instance = new SSHRemote();
		}
		return instance;
	}
	
	private void execSSHCmd(String cmd, StringBuilder buf)
	{
		boolean end = false;
		String tmpStr;
		String tmpCheckStr;
		int timeout;
		boolean cmdReceivedFlg;

		if(session.isConnected() == false)
		{
			return;
		}

		try {
			pin.write(cmd.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			tmpStr= "";
			tmpCheckStr = "";
			timeout = 0;
			cmdReceivedFlg = false;
			int ch;
			while(!end)
			{
				if(consoleOutput.ready() == true)
				{
					ch = consoleOutput.read();
					if(ch != -1)
					{
						tmpStr = tmpStr + Character.toString((char)ch);
						tmpCheckStr = tmpStr.replace("\n", "").replace("\r", "");
						String cmdComp = cmd.replace("\n", "").replace("\0", "");
						if(tmpCheckStr.contains(cmdComp))
						{
							cmdReceivedFlg = true;
							if(tmpStr.endsWith(promptString) || tmpStr.endsWith(nlPromptString1) || tmpStr.endsWith(nlPromptString2))
							{
								//The command was executed. Nothing more to wait for.
								if(buf != null)
								{
									buf.append(tmpStr);
								}
								end = true;
							}
						}
					}
				}
				else
				{
					timeout++;
					if((tmpStr.length() > 0) && (timeout > 20))
					{
						//add Ctrl+C for next step. After this checkfor prompt only!
						pin.write(0x03);
					}
					Thread.sleep(50);
				}
				if(timeout > 40)
				{
					end = true;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void sshSetPrompt()
	{
		String tmpPromptString;
		StringBuilder s = new StringBuilder();
		tmpPromptString = "PS1=\"" + promptString + "\"\n\0";   
		execSSHCmd(tmpPromptString, s);
	}
	
	public ArrayList<String> refreshFolders()
	{
		ArrayList<String> out = new ArrayList<String>();
		StringBuilder s = new StringBuilder();
		execSSHCmd("pwd\n", s);
		String[] sl = s.toString().split("\n", 0);
		if(sl.length > 0)
		{
//TODO:			ui.lblCurPath->setText(sl.at(1));
		}
		s.setLength(0);
		execSSHCmd("ls -1 --color=none\n", s);
		sl =  s.toString().split("\n", 0);
		out.clear();
		out.add("..");
		for(String st: sl)
		{
			if((!st.contains("ls -1 --color=none")) && (!st.contains(promptString)))
			{
				out.add(st.replace("\n","").replace("\r",""));
			}
		}
		return out;
	}

	public void connectHost(String hostAddress, String usrName, String userPassword){ 
		try{
			//jsch.setKnownHosts("/home/foo/.ssh/known_hosts");
			JSch.setConfig("StrictHostKeyChecking", "no");
			session=jsch.getSession(usrName, hostAddress, 22);
			session.setPassword(userPassword);	 
///			session.setUserInfo(ui);
	 
			// It must not be recommended, but if you want to skip host-key check,
			// invoke following,
			// session.setConfig("StrictHostKeyChecking", "no");
	 
			//session.connect();
			session.connect(30000);   // making a connection with timeout.
	 
			channel=session.openChannel("shell");
	 
			// Enable agent-forwarding.
			//((ChannelShell)channel).setAgentForwarding(true);
	 
			channel.setInputStream(in);
	 
			channel.setOutputStream(out);
	 
	      // Choose the pty-type "vt102".
	      ((ChannelShell)channel).setPtyType("vt102");
	 
	      /*
	      // Set environment variable "LANG" as "ja_JP.eucJP".
	      ((ChannelShell)channel).setEnv("LANG", "ja_JP.eucJP");
	      */
	 
	      //channel.connect();
			channel.connect(3*1000);
			sshSetPrompt();
			String cmd = "export DISPLAY=:0\n";
			StringBuilder s = new StringBuilder();
		 	execSSHCmd(cmd, s);

			checkMplayerRunning();
//			uiEnableConnected();
			refreshFolders();

		}
		catch(Exception e){
			System.out.println(e);
		}
	}
	
	public boolean isConnected(){
		return session.isConnected();
	}
	
	public String sendCommand(String cmdString)
	{
		StringBuilder s = new StringBuilder();
		if(isConnected())
		{
			execSSHCmd(cmdString+"\n", s);
		}
		return s.toString();
	}

	public void cdOrOpen(String path)
	{
		String out = sendCommand("cd " + path);
		if(out.contains("Not a directory"))
		{
			String fname = sendCommand("pwd");
			fname = fname.replace("pwd", "");
			fname = fname.replace("\n", "");
			fname = fname.replace("\r", "");
			fname = fname.replace(promptString, "");
			fname = fname.replace(" ", "\\ ");
			fname = fname.replace("(", "\\(");
			fname = fname.replace(")", "\\)");
			fname = fname.replace("'", "\\'");
			fname = fname + "/" + path;
			sendOpen(fname);
		}
	}

	public void sendFullScreen()
	{
		String openCmd = "dbus-send --type=signal / com.gnome.mplayer.SetFullscreen boolean:'true'";
		sendCommand(openCmd);
	}

	public void sendNormalScreen()
	{
		String openCmd = "dbus-send --type=signal / com.gnome.mplayer.SetFullscreen boolean:'false'";
		sendCommand(openCmd);
	}

	public void sendPlay()
	{
		String openCmd = "dbus-send / com.gnome.mplayer.Play";
		sendCommand(openCmd);
	}

	public void sendPause()
	{
		String openCmd = "dbus-send / com.gnome.mplayer.Pause";
		sendCommand(openCmd);
	}

	public void sendStop()
	{
		String openCmd = "dbus-send / com.gnome.mplayer.Stop";
		sendCommand(openCmd);
	}

	public void sendVolumeUp()
	{
		String openCmd = "dbus-send / com.gnome.mplayer.VolumeUp";
		sendCommand(openCmd);
	}

	public void sendVolumeDown()
	{
		String openCmd = "dbus-send / com.gnome.mplayer.VolumeDown";
		sendCommand(openCmd);
	}

	public void sendOpen(String path)
	{
		String openCmd = "dbus-send --type=signal / com.gnome.mplayer.Open string:";
		StringBuilder s = new StringBuilder();
		openCmd = openCmd + "'" + path + "'\n";  
		execSSHCmd(openCmd, s);
	}
	
	public void startmplayerClicked()
	{
		String cmd = "gnome-mplayer &\n";
		StringBuilder s = new StringBuilder();
	 	execSSHCmd(cmd, s);
		checkMplayerRunning();
	}

	public ArrayList<String> getMPlayerRunning()
	{
		String cmd = "pgrep gnome-mplayer\n";
		StringBuilder sb = new StringBuilder();
		ArrayList<String> out = new ArrayList<String>();
		execSSHCmd(cmd, sb);
		String[] mr =  sb.toString().replace(cmd.replace("\n",""),"").replace(promptString,"").replace("\r","").split("\n", 0);
		for (String ins: mr)
		{
			if((ins != null) && (ins.length() > 0))
			{
				out.add(ins);
			}
		}
		return out;
	}

	public void checkMplayerRunning()
	{
		ArrayList<String> mp = getMPlayerRunning();
		if(mp.isEmpty())
		{
			//ui.pbStartMPlayer->setEnabled(true);
			//ui.pbKillMPlayer->setEnabled(false);
		}
		else
		{
//			ui.pbStartMPlayer->setEnabled(false);
//			ui.pbKillMPlayer->setEnabled(true);
			StringBuilder s = new StringBuilder();
			String cmd = "$(tr '\\0' '\\n' < /proc/$(pgrep gnome-mplayer | head -1 )/environ | sed -e 's/^/export /')\n";
	 		execSSHCmd(cmd, s);
			cmd = "echo $DBUS_SESSION_BUS_ADDRESS\n";
	 		execSSHCmd(cmd, s);
		}
	}

}
